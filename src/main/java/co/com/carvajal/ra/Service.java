package co.com.carvajal.ra;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonRootName;



@org.kie.api.definition.type.Label("Service")
@JsonRootName(value = "service")
public class Service implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private Long idServiceRequest;
	private Long idItemOrder;
	private String serviceCode;
	private String serviceName;

	private Long idPersonPrincipal; // Dueno de la orden
	private Boolean thirdPerson; // indica si es tercera persona

	private Boolean confront;
	private String validationType;
	private Integer biometryRadiated;
	private Boolean acceptContractSubscription;
	private String state;
	private String validationCode;

	private String firstName;
	private String secondName;
	private String firstSurname;
	private String secondSurname;

	private String requestNumber;
	private Date requestDate;
	private String address;
	private String cellPhone;
	private String email;

	private String countryCode;
	private String countryName;
	private String departmentCode;
	private String departmentName;
	private String municipalityCode;
	private String municipalityName;
	private String city;

	private String mmre; // migracion colombia
	private String sigep; // sigep entidad
	private String rues;
	private String aspr;

	private String rejectionReason;
	private Date rejectionDate;
	private String reasonDecline;

	private String typeIdentificationEntity;
	private String identificationEntity;
	private String typeEntity;
	private String nameEntity;
	private String registryEntity;

	private String positionHeld; // cargo del suscriptor servicio

	private String typeFunctionaryCode;
    private String typeFunctionaryName;
    private String typeContractCode;
    private String typeContractName;
	private Date dateBeginContract;
	private Date dateFinishContract;
	private String representationTypeCode;
    private String representationTypeName;
	private String otherRepresentation;

	private String professionalAssociationCode;
	private String professionalAssociationName;
	private String otherProfessionalAssociation;
	private String professionalTitle;
	private String professionalRegistration;
	private Date dateRegistration;
	private String institution;

	private String userSession;
	private boolean action;
	private String observation;

	public Service() {

	}

	public Long getIdServiceRequest() {
		return idServiceRequest;
	}

	public void setIdServiceRequest(Long idServiceRequest) {
		this.idServiceRequest = idServiceRequest;
	}

	public Long getIdItemOrder() {
		return idItemOrder;
	}

	public void setIdItemOrder(Long idItemOrder) {
		this.idItemOrder = idItemOrder;
	}

	public String getServiceCode() {
		return serviceCode;
	}

	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public Long getIdPersonPrincipal() {
		return idPersonPrincipal;
	}

	public void setIdPersonPrincipal(Long idPersonPrincipal) {
		this.idPersonPrincipal = idPersonPrincipal;
	}

	public Boolean getThirdPerson() {
		return thirdPerson;
	}

	public void setThirdPerson(Boolean thirdPerson) {
		this.thirdPerson = thirdPerson;
	}

	public Boolean getConfront() {
		return confront;
	}

	public void setConfront(Boolean confront) {
		this.confront = confront;
	}

	public String getValidationType() {
		return validationType;
	}

	public void setValidationType(String validationType) {
		this.validationType = validationType;
	}

	public Integer getBiometryRadiated() {
		return biometryRadiated;
	}

	public void setBiometryRadiated(Integer biometryRadiated) {
		this.biometryRadiated = biometryRadiated;
	}

	public Boolean getAcceptContractSubscription() {
		return acceptContractSubscription;
	}

	public void setAcceptContractSubscription(Boolean acceptContractSubscription) {
		this.acceptContractSubscription = acceptContractSubscription;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getValidationCode() {
		return validationCode;
	}

	public void setValidationCode(String validationCode) {
		this.validationCode = validationCode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getFirstSurname() {
		return firstSurname;
	}

	public void setFirstSurname(String firstSurname) {
		this.firstSurname = firstSurname;
	}

	public String getSecondSurname() {
		return secondSurname;
	}

	public void setSecondSurname(String secondSurname) {
		this.secondSurname = secondSurname;
	}

	public String getRequestNumber() {
		return requestNumber;
	}

	public void setRequestNumber(String requestNumber) {
		this.requestNumber = requestNumber;
	}

	public Date getRequestDate() {
		return requestDate;
	}

	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getMunicipalityCode() {
		return municipalityCode;
	}

	public void setMunicipalityCode(String municipalityCode) {
		this.municipalityCode = municipalityCode;
	}

	public String getMunicipalityName() {
		return municipalityName;
	}

	public void setMunicipalityName(String municipalityName) {
		this.municipalityName = municipalityName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getMmre() {
		return mmre;
	}

	public void setMmre(String mmre) {
		this.mmre = mmre;
	}

	public String getSigep() {
		return sigep;
	}

	public void setSigep(String sigep) {
		this.sigep = sigep;
	}

	public String getRues() {
		return rues;
	}

	public void setRues(String rues) {
		this.rues = rues;
	}

	public String getAspr() {
		return aspr;
	}

	public void setAspr(String aspr) {
		this.aspr = aspr;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	public Date getRejectionDate() {
		return rejectionDate;
	}

	public void setRejectionDate(Date rejectionDate) {
		this.rejectionDate = rejectionDate;
	}

	public String getReasonDecline() {
		return reasonDecline;
	}

	public void setReasonDecline(String reasonDecline) {
		this.reasonDecline = reasonDecline;
	}

	public String getTypeIdentificationEntity() {
		return typeIdentificationEntity;
	}

	public void setTypeIdentificationEntity(String typeIdentificationEntity) {
		this.typeIdentificationEntity = typeIdentificationEntity;
	}

	public String getIdentificationEntity() {
		return identificationEntity;
	}

	public void setIdentificationEntity(String identificationEntity) {
		this.identificationEntity = identificationEntity;
	}

	public String getTypeEntity() {
		return typeEntity;
	}

	public void setTypeEntity(String typeEntity) {
		this.typeEntity = typeEntity;
	}

	public String getNameEntity() {
		return nameEntity;
	}

	public void setNameEntity(String nameEntity) {
		this.nameEntity = nameEntity;
	}

	public String getRegistryEntity() {
		return registryEntity;
	}

	public void setRegistryEntity(String registryEntity) {
		this.registryEntity = registryEntity;
	}

	public String getPositionHeld() {
		return positionHeld;
	}

	public void setPositionHeld(String positionHeld) {
		this.positionHeld = positionHeld;
	}

	public String getTypeFunctionaryCode() {
		return typeFunctionaryCode;
	}

	public void setTypeFunctionaryCode(String typeFunctionaryCode) {
		this.typeFunctionaryCode = typeFunctionaryCode;
	}

	public String getTypeContractCode() {
		return typeContractCode;
	}

	public void setTypeContractCode(String typeContractCode) {
		this.typeContractCode = typeContractCode;
	}

	public Date getDateBeginContract() {
		return dateBeginContract;
	}

	public void setDateBeginContract(Date dateBeginContract) {
		this.dateBeginContract = dateBeginContract;
	}

	public Date getDateFinishContract() {
		return dateFinishContract;
	}

	public void setDateFinishContract(Date dateFinishContract) {
		this.dateFinishContract = dateFinishContract;
	}

	public String getRepresentationTypeCode() {
		return representationTypeCode;
	}

	public void setRepresentationTypeCode(String representationTypeCode) {
		this.representationTypeCode = representationTypeCode;
	}

	public String getOtherRepresentation() {
		return otherRepresentation;
	}

	public void setOtherRepresentation(String otherRepresentation) {
		this.otherRepresentation = otherRepresentation;
	}

	public String getProfessionalAssociationCode() {
		return professionalAssociationCode;
	}

	public void setProfessionalAssociationCode(
			String professionalAssociationCode) {
		this.professionalAssociationCode = professionalAssociationCode;
	}

	public String getProfessionalAssociationName() {
		return professionalAssociationName;
	}

	public void setProfessionalAssociationName(
			String professionalAssociationName) {
		this.professionalAssociationName = professionalAssociationName;
	}

	public String getOtherProfessionalAssociation() {
		return otherProfessionalAssociation;
	}

	public void setOtherProfessionalAssociation(
			String otherProfessionalAssociation) {
		this.otherProfessionalAssociation = otherProfessionalAssociation;
	}

	public String getProfessionalTitle() {
		return professionalTitle;
	}

	public void setProfessionalTitle(String professionalTitle) {
		this.professionalTitle = professionalTitle;
	}

	public String getProfessionalRegistration() {
		return professionalRegistration;
	}

	public void setProfessionalRegistration(String professionalRegistration) {
		this.professionalRegistration = professionalRegistration;
	}

	public Date getDateRegistration() {
		return dateRegistration;
	}

	public void setDateRegistration(Date dateRegistration) {
		this.dateRegistration = dateRegistration;
	}

	public String getInstitution() {
		return institution;
	}

	public void setInstitution(String institution) {
		this.institution = institution;
	}

	public String getUserSession() {
		return userSession;
	}

	public void setUserSession(String userSession) {
		this.userSession = userSession;
	}

	public boolean isAction() {
		return action;
	}

	public void setAction(boolean action) {
		this.action = action;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}
	
	 public String getTypeFunctionaryName() {
        return typeFunctionaryName;
    }

    public void setTypeFunctionaryName(String typeFunctionaryName) {
        this.typeFunctionaryName = typeFunctionaryName;
    }

    public String getTypeContractName() {
        return typeContractName;
    }

    public void setTypeContractName(String typeContractName) {
        this.typeContractName = typeContractName;
    }

    public String getRepresentationTypeName() {
        return representationTypeName;
    }

    public void setRepresentationTypeName(String representationTypeName) {
        this.representationTypeName = representationTypeName;
    }

	@Override
    public String toString() {
        return "Service [idServiceRequest=" + idServiceRequest
                + ", idItemOrder=" + idItemOrder + ", serviceCode="
                + serviceCode + ", serviceName=" + serviceName
                + ", idPersonPrincipal=" + idPersonPrincipal + ", thirdPerson="
                + thirdPerson + ", confront=" + confront + ", validationType="
                + validationType + ", biometryRadiated=" + biometryRadiated
                + ", acceptContractSubscription=" + acceptContractSubscription
                + ", state=" + state + ", validationCode=" + validationCode
                + ", firstName=" + firstName + ", secondName=" + secondName
                + ", firstSurname=" + firstSurname + ", secondSurname="
                + secondSurname + ", requestNumber=" + requestNumber
                + ", requestDate=" + requestDate + ", address=" + address
                + ", cellPhone=" + cellPhone + ", email=" + email
                + ", countryCode=" + countryCode + ", countryName="
                + countryName + ", departmentCode=" + departmentCode
                + ", departmentName=" + departmentName + ", municipalityCode="
                + municipalityCode + ", municipalityName=" + municipalityName
                + ", city=" + city + ", mmre=" + mmre + ", sigep=" + sigep
                + ", rues=" + rues + ", aspr=" + aspr + ", rejectionReason="
                + rejectionReason + ", rejectionDate=" + rejectionDate
                + ", reasonDecline=" + reasonDecline
                + ", typeIdentificationEntity=" + typeIdentificationEntity
                + ", identificationEntity=" + identificationEntity
                + ", typeEntity=" + typeEntity + ", nameEntity=" + nameEntity
                + ", registryEntity=" + registryEntity + ", positionHeld="
                + positionHeld + ", typeFunctionaryCode=" + typeFunctionaryCode
                + ", typeFunctionaryName=" + typeFunctionaryName
                + ", typeContractCode=" + typeContractCode
                + ", typeContractName=" + typeContractName
                + ", dateBeginContract=" + dateBeginContract
                + ", dateFinishContract=" + dateFinishContract
                + ", representationTypeCode=" + representationTypeCode
                + ", representationTypeName=" + representationTypeName
                + ", otherRepresentation=" + otherRepresentation
                + ", professionalAssociationCode=" + professionalAssociationCode
                + ", professionalAssociationName=" + professionalAssociationName
                + ", otherProfessionalAssociation="
                + otherProfessionalAssociation + ", professionalTitle="
                + professionalTitle + ", professionalRegistration="
                + professionalRegistration + ", dateRegistration="
                + dateRegistration + ", institution=" + institution
                + ", userSession=" + userSession + ", action=" + action
                + ", observation=" + observation + "]";
    }

	public Service(Long idServiceRequest, Long idItemOrder,
            String serviceCode, String serviceName, Long idPersonPrincipal,
            Boolean thirdPerson, Boolean confront, String validationType,
            Integer biometryRadiated, Boolean acceptContractSubscription,
            String state, String validationCode, String firstName,
            String secondName, String firstSurname, String secondSurname,
            String requestNumber, Date requestDate, String address,
            String cellPhone, String email, String countryCode,
            String countryName, String departmentCode, String departmentName,
            String municipalityCode, String municipalityName, String city,
            String mmre, String sigep, String rues, String aspr,
            String rejectionReason, Date rejectionDate, String reasonDecline,
            String typeIdentificationEntity, String identificationEntity,
            String typeEntity, String nameEntity, String registryEntity,
            String positionHeld, String typeFunctionaryCode,
            String typeFunctionaryName, String typeContractCode,
            String typeContractName, Date dateBeginContract,
            Date dateFinishContract, String representationTypeCode,
            String representationTypeName, String otherRepresentation,
            String professionalAssociationCode,
            String professionalAssociationName,
            String otherProfessionalAssociation, String professionalTitle,
            String professionalRegistration, Date dateRegistration,
            String institution, String userSession, boolean action,
            String observation) {
        super();
        this.idServiceRequest = idServiceRequest;
        this.idItemOrder = idItemOrder;
        this.serviceCode = serviceCode;
        this.serviceName = serviceName;
        this.idPersonPrincipal = idPersonPrincipal;
        this.thirdPerson = thirdPerson;
        this.confront = confront;
        this.validationType = validationType;
        this.biometryRadiated = biometryRadiated;
        this.acceptContractSubscription = acceptContractSubscription;
        this.state = state;
        this.validationCode = validationCode;
        this.firstName = firstName;
        this.secondName = secondName;
        this.firstSurname = firstSurname;
        this.secondSurname = secondSurname;
        this.requestNumber = requestNumber;
        this.requestDate = requestDate;
        this.address = address;
        this.cellPhone = cellPhone;
        this.email = email;
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.departmentCode = departmentCode;
        this.departmentName = departmentName;
        this.municipalityCode = municipalityCode;
        this.municipalityName = municipalityName;
        this.city = city;
        this.mmre = mmre;
        this.sigep = sigep;
        this.rues = rues;
        this.aspr = aspr;
        this.rejectionReason = rejectionReason;
        this.rejectionDate = rejectionDate;
        this.reasonDecline = reasonDecline;
        this.typeIdentificationEntity = typeIdentificationEntity;
        this.identificationEntity = identificationEntity;
        this.typeEntity = typeEntity;
        this.nameEntity = nameEntity;
        this.registryEntity = registryEntity;
        this.positionHeld = positionHeld;
        this.typeFunctionaryCode = typeFunctionaryCode;
        this.typeFunctionaryName = typeFunctionaryName;
        this.typeContractCode = typeContractCode;
        this.typeContractName = typeContractName;
        this.dateBeginContract = dateBeginContract;
        this.dateFinishContract = dateFinishContract;
        this.representationTypeCode = representationTypeCode;
        this.representationTypeName = representationTypeName;
        this.otherRepresentation = otherRepresentation;
        this.professionalAssociationCode = professionalAssociationCode;
        this.professionalAssociationName = professionalAssociationName;
        this.otherProfessionalAssociation = otherProfessionalAssociation;
        this.professionalTitle = professionalTitle;
        this.professionalRegistration = professionalRegistration;
        this.dateRegistration = dateRegistration;
        this.institution = institution;
        this.userSession = userSession;
        this.action = action;
        this.observation = observation;
    }

}