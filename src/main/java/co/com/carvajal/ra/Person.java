package co.com.carvajal.ra;

import java.util.Date;
import java.util.List;
import co.com.carvajal.ra.PersonRole;

import com.fasterxml.jackson.annotation.JsonRootName;


@org.kie.api.definition.type.Label("Person")
@JsonRootName(value = "person")
public class Person implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private Long idPerson;
	private String identificationType;
	private String identification;
	private String firstName;
	private String secondName;
	private String firstSurname;
	private String secondSurname;
	private String suscriptionType;
	private Boolean policies;
	private String biometricsState;
	private Date biometricsStateDate;
	private String ani;
	private Long idContactDetails;
	private String phone;
	private String cellPhone;
	private String email;
	private Boolean notifications;
	private Boolean publicity;
	private String cellPhoneCode;
	private String userSession;
	@org.kie.api.definition.type.Label("roles")
	private List<PersonRole> roles;

	public Person() {
		super();
	}

	public Person(Long idPerson, String identificationType,
			String identification, String firstName, String secondName,
			String firstSurname, String secondSurname, String suscriptionType,
			Boolean policies, String biometricsState, Date biometricsStateDate,
			String ani, Long idContactDetails, String phone, String cellPhone,
			String email, Boolean notifications, Boolean publicity,
			String cellPhoneCode, String userSession, List<PersonRole> roles) {
		super();
		this.idPerson = idPerson;
		this.identificationType = identificationType;
		this.identification = identification;
		this.firstName = firstName;
		this.secondName = secondName;
		this.firstSurname = firstSurname;
		this.secondSurname = secondSurname;
		this.suscriptionType = suscriptionType;
		this.policies = policies;
		this.biometricsState = biometricsState;
		this.biometricsStateDate = biometricsStateDate;
		this.ani = ani;
		this.idContactDetails = idContactDetails;
		this.phone = phone;
		this.cellPhone = cellPhone;
		this.email = email;
		this.notifications = notifications;
		this.publicity = publicity;
		this.cellPhoneCode = cellPhoneCode;
		this.userSession = userSession;
		this.roles = roles;
	}

	public Long getIdPerson() {
		return idPerson;
	}

	public void setIdPerson(Long idPerson) {
		this.idPerson = idPerson;
	}

	public String getIdentificationType() {
		return identificationType;
	}

	public void setIdentificationType(String identificationType) {
		this.identificationType = identificationType;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getFirstSurname() {
		return firstSurname;
	}

	public void setFirstSurname(String firstSurname) {
		this.firstSurname = firstSurname;
	}

	public String getSecondSurname() {
		return secondSurname;
	}

	public void setSecondSurname(String secondSurname) {
		this.secondSurname = secondSurname;
	}

	public String getSuscriptionType() {
		return suscriptionType;
	}

	public void setSuscriptionType(String suscriptionType) {
		this.suscriptionType = suscriptionType;
	}

	public Boolean getPolicies() {
		return policies;
	}

	public void setPolicies(Boolean policies) {
		this.policies = policies;
	}

	public String getBiometricsState() {
		return biometricsState;
	}

	public void setBiometricsState(String biometricsState) {
		this.biometricsState = biometricsState;
	}

	public Date getBiometricsStateDate() {
		return biometricsStateDate;
	}

	public void setBiometricsStateDate(Date biometricsStateDate) {
		this.biometricsStateDate = biometricsStateDate;
	}

	public String getAni() {
		return ani;
	}

	public void setAni(String ani) {
		this.ani = ani;
	}

	public Long getIdContactDetails() {
		return idContactDetails;
	}

	public void setIdContactDetails(Long idContactDetails) {
		this.idContactDetails = idContactDetails;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getNotifications() {
		return notifications;
	}

	public void setNotifications(Boolean notifications) {
		this.notifications = notifications;
	}

	public Boolean getPublicity() {
		return publicity;
	}

	public void setPublicity(Boolean publicity) {
		this.publicity = publicity;
	}

	public String getCellPhoneCode() {
		return cellPhoneCode;
	}

	public void setCellPhoneCode(String cellPhoneCode) {
		this.cellPhoneCode = cellPhoneCode;
	}

	public String getUserSession() {
		return userSession;
	}

	public void setUserSession(String userSession) {
		this.userSession = userSession;
	}

	public List<PersonRole> getRoles() {
		return roles;
	}

	public void setRoles(List<PersonRole> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Person [idPerson=" + idPerson + ", identificationType="
				+ identificationType + ", identification=" + identification
				+ ", firstName=" + firstName + ", secondName=" + secondName
				+ ", firstSurname=" + firstSurname + ", secondSurname="
				+ secondSurname + ", suscriptionType=" + suscriptionType
				+ ", policies=" + policies + ", biometricsState="
				+ biometricsState + ", biometricsStateDate="
				+ biometricsStateDate + ", ani=" + ani + ", idContactDetails="
				+ idContactDetails + ", phone=" + phone + ", cellPhone="
				+ cellPhone + ", email=" + email + ", notifications="
				+ notifications + ", publicity=" + publicity
				+ ", cellPhoneCode=" + cellPhoneCode + ", userSession="
				+ userSession + ", roles=" + roles + "]";
	}

}