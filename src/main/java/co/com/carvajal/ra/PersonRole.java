package co.com.carvajal.ra;

import com.fasterxml.jackson.annotation.JsonRootName;


@JsonRootName(value = "roles")
public class PersonRole implements java.io.Serializable {

	static final long serialVersionUID = 1L;
    private Long idPersonRole;
    private String codeRole;
    private String state;
    private String createUser;
    private String updateUser;

    public PersonRole(Long idPersonRole, String codeRole, String state, String createUser) {
        this.idPersonRole = idPersonRole;
        this.codeRole = codeRole;
        this.state = state;
        this.createUser = createUser;
    }
    
    public PersonRole(){
        
    }

    public Long getIdPersonRole() {
        return idPersonRole;
    }

    public void setIdPersonRole(Long idPersonRole) {
        this.idPersonRole = idPersonRole;
    }

    public String getCodeRole() {
        return codeRole;
    }

    public void setCodeRole(String codeRole) {
        this.codeRole = codeRole;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
    
        @Override
    public String toString() {
        return "PersonRoleDTO{" + "idPersonRole=" + idPersonRole + ", codeRole=" + codeRole + ", state=" + state + ", createUser=" + createUser + ", updateUser=" + updateUser + '}';
    }
    

}